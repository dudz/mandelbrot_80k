/*
Cria imagem inicial para fractal gigante de mandelbrot com dimensões
80001 x 80001.

É criada a linha central, e as demais são pretas para posterior preenchimento
de cores de forma otimizada com agrupamento de grandes blocos pretos.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "rotinas_comuns.h"

#define QTD_TESTES (1000000)

#define NOME_ARQUIVO_TMP "mandel80k.bmp.tmp"

extern void estado_inicial_nivel_3(void);
extern void estado_inicial_nivel_2(void);
extern void estado_inicial_nivel_1(void);


void grava_cabecalho_bmp (FILE *fd, int32_t tamanho) {
    uint8_t paleta[16][3] = {
        {  0,  0,  0},
        {255,255,255},
        {255,255,  0},
        {  0,255,255},
        {255,  0,255},
        {  0,  0,255},
        {  0,255,  0},
        {255,  0,  0},
        {192,192,192},
        {128,128,128},
        {  0,128,128},
        {128,  0,128},
        {128,128,  0},
        {  0,  0,128},
        {  0,128,  0},
        {128,  0,  0}
    };
    uint16_t buf2;
    uint32_t buf4;
    int i, j;
	const int qtd_cores = 16;
	const int bits_por_pixel = 4;
	const int tamanho_cabecalho_bmp = 14;
	const int tamanho_header_versao_bmp = 40; /* tamanho BITMAPINFOHEADER */
	const int bytes_por_cor = 4;

    /* Bitmap file header */
    buf2 = 0x4d42; /* "BM" em little-endian */
    fwrite(&buf2, 2, 1, fd);
    buf4 = (uint32_t)tamanho_cabecalho_bmp + (uint32_t)tamanho_header_versao_bmp + (uint32_t)(qtd_cores * bytes_por_cor) +
		((uint32_t) tamanho * (uint32_t)((tamanho * bits_por_pixel + 31) / 32) * (uint32_t) 4); /* tamanho arquivo (padding para ser múltiplo de 4 em cada linha) */
    fwrite(&buf4, 4, 1, fd);
    buf4 = 0; /* dois campos reservados de 16 bits cada */
    fwrite(&buf4, 4, 1, fd);
    buf4 = tamanho_cabecalho_bmp + tamanho_header_versao_bmp + (qtd_cores * bytes_por_cor); /* deslocamento até os pixels */
    fwrite(&buf4, 4, 1, fd);

    /* BITMAPINFOHEADER */
    buf4 = tamanho_header_versao_bmp;
    fwrite(&buf4, 4, 1, fd);
    buf4 = tamanho; /* largura */
    fwrite(&buf4, 4, 1, fd);
    buf4 = tamanho; /* altura */
    fwrite(&buf4, 4, 1, fd);
    buf2 = 1; /* qtd planos */
    fwrite(&buf2, 2, 1, fd);
    buf2 = bits_por_pixel; /* bits por pixel */
    fwrite(&buf2, 2, 1, fd);
    buf4 = 0; /* compressão BI_RGB (sem compressão) */
    fwrite(&buf4, 4, 1, fd);
    buf4 = 0; /* imagesize (pode ser 0 para compressão BI_RGB) */
    fwrite(&buf4, 4, 1, fd);
    buf4 = tamanho; /* resolução horizontal e vertical (pixels por metro) */
    fwrite(&buf4, 4, 1, fd);
    fwrite(&buf4, 4, 1, fd);
    buf4 = qtd_cores; /* qtd cores na paleta */
    fwrite(&buf4, 4, 1, fd);
    buf4 = qtd_cores; /* qtd cores importantes */
    fwrite(&buf4, 4, 1, fd);

    /* paleta */
    for (i = 0; i < qtd_cores; i++) {
        for (j = 0; j < 3; j++) fputc(paleta[i][2 - j], fd); /* ordem BGR */
        fputc(0, fd);
    }
    fflush (fd);
}

void cria_imagem_inicial (void) {

	int i, j;
	FILE *fd;
	uint8_t *linha;
	uint8_t *buffer_gravacao;
	double tamanho_pixel;
	double x;

	if (file_exists(NOME_ARQUIVO_TMP)) {
		fprintf (stderr, "%s ja existe. Apague se quiser reiniciar.\n", NOME_ARQUIVO_TMP);
		exit(1);
	}

	linha = (uint8_t *) malloc(80001);
	buffer_gravacao = (uint8_t *) malloc(40005);
	if (linha == NULL || buffer_gravacao == NULL) {
		if (linha != NULL) free (linha);
		if (buffer_gravacao != NULL) free (buffer_gravacao);
		fprintf (stderr, "Sem memoria.\n");
		exit(1);
	}
	
	fd = fopen(NOME_ARQUIVO_TMP, "wb");
	if (fd == NULL) {
		fprintf (stderr, "Erro ao criar arquivo %s.\n", NOME_ARQUIVO_TMP);
		exit(1);
	}

	grava_cabecalho_bmp (fd, 80001);

	/* ultimas (bmp tem ordem inversa) 40000 linhas em ciano (não usar a cor 0 para não mascarar erros) */
	for (j = 0; j < 80001; j++) linha[j] = 3;
	for (i = 0; i < 40000; i++) {
		grava_linha_bmp_4bpp (fd, linha, buffer_gravacao, 80001);
	}

	/* linha central será a única calculada */
	/* ate coordenada (0.25, 0.0), equivalente à coluna 45000, eh preto com certeza */
	for (j = 0; j < 45000; j++) linha[j] = 0;
	tamanho_pixel = 4.0 / 80001;
	while (j < 80001) {
		x = (j - 40000) * tamanho_pixel;
		linha[j] = cor_pixel(x, 0.0, QTD_TESTES);
		j++;
	}
	grava_linha_bmp_4bpp (fd, linha, buffer_gravacao, 80001);
	
	/* primeiras (bmp tem ordem inversa) 40000 linhas em ciano (não usar a cor 0 para não mascarar erros) */
	for (j = 0; j < 80001; j++) linha[j] = 3;
	for (i = 0; i < 40000; i++) {
		grava_linha_bmp_4bpp (fd, linha, buffer_gravacao, 80001);
	}

	if (linha != NULL) free (linha);
	if (buffer_gravacao != NULL) free (buffer_gravacao);
	
	fclose (fd);
	
}

void grava_linha_inicial (void) {
	FILE *fd;
	if (file_exists("linha_atual.dat")) {
		fprintf(stderr, "linha_atual.dat ja existe. Apague se quiser reiniciar.\n");
		exit(1);
	}
	fd = fopen("linha_atual.dat", "w");
	if (fd == NULL) {
		fprintf(stderr, "Erro ao criar linha_atual.dat.\n");
		exit(1);
	}
	fprintf (fd, "0\n");
	fclose (fd);
}

void cria_estado_inicial (void) {
	
	printf ("Criando imagem inicial.\n");
	cria_imagem_inicial	();
	printf ("Criando grade de otimizacao nivel 3.\n");
	estado_inicial_nivel_3();
	printf ("Criando grade de otimizacao nivel 2.\n");
	estado_inicial_nivel_2();
	printf ("Criando grade de otimizacao nivel 1.\n");
	estado_inicial_nivel_1();
	
	grava_linha_inicial();

}
