#include "rotinas_comuns.h"


/*
Devolve 0 para ponto do conjunto, e para pontos fora do
conjunto devolve o número de passos até passar de 2.0, limitado
a 4*14 cores (circulares na paleta) no máximo (passando disso é branco).
Cores que passariam de 15 são rotacionadas
(quatro conjuntos das 14 cores, tirando branco e preto).
Pontos muito próximos do conjunto são brancos (cor 1), assim como
a área externa do círculo de raio 2.
*/
uint8_t cor_pixel(double x, double y, int32_t qtd_testes) {
    double xx, yy;
    double cx, cy;
    int qtd_passos;

    if (x * x + y * y >= 4.0) return 1;
    cx = x;
    cy = y;
    /* (x + yi) ^2 =  (x^2 - y^2) + 2xyi */
    qtd_passos = -1;
    while(qtd_testes-- > 0) {
        if (qtd_passos < 56) qtd_passos++;
        xx = x*x - y*y + cx;
        yy = 2.0 * x * y + cy;
        x = xx;
        y = yy;
        if (x * x + y * y >= 4.0) {
            if (qtd_passos >= 56) return 1;
            qtd_passos = qtd_passos % 14;
            return qtd_passos + 2;
        }
    }
    return 0;
}

bool file_exists(char *filename) {
	FILE *fd;
	if (filename == NULL) return false;
	fd = fopen (filename, "rb");
	if (fd != NULL) {
		fclose (fd);
		return true;
	}
	return false;
}

/* grava as cores em linha no arquivo, usando espaço auxiliar buffer_gravacao. tamanho é a quantidade de colunas */
void grava_linha_bmp_4bpp (FILE *fd, uint8_t *linha, uint8_t *buffer_gravacao, int tamanho) {
	
	int i, j;

	j = 0; /* indice do buffer de gravacao */
	for (i = 1; i < tamanho; i += 2) {
		buffer_gravacao[j] = (linha[i - 1] << 4) | linha[i];
		j++;
	}
	if ((tamanho & 1) != 0) { /* tamanho impar tem pad inicial de 4 bits */
		buffer_gravacao[j] = linha[tamanho - 1] << 4;
		j++;
		tamanho++;
	}
	tamanho = ((tamanho * 4 + 31) / 32) * 4; /* qtd bytes a gravar */
	while (j < tamanho) buffer_gravacao[j++] = 0; /* pad */
	fwrite(buffer_gravacao, tamanho, 1, fd);
}
