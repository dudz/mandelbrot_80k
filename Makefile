CFLAGS = -Wall -Wextra -pedantic -O2 -s
CFLAGS2 = -Wall -Wextra -pedantic -g

all: mandelbrot_80k.exe

cria_estado_inicial.o: cria_estado_inicial.c
	gcc $(CFLAGS) -c -o $@ $<

rotinas_comuns.o: rotinas_comuns.c rotinas_comuns.h
	gcc $(CFLAGS) -c -o $@ $<

mt19937ar.o: mt19937ar.c mt19937ar.h
	gcc $(CFLAGS) -c -o $@ $<

nivel_3.o: nivel_3.c
	gcc $(CFLAGS) -c -o $@ $^

nivel_2.o: nivel_2.c
	gcc $(CFLAGS) -c -o $@ $^

nivel_1.o: nivel_1.c
	gcc $(CFLAGS) -c -o $@ $^

mandelbrot_80k.exe: mandelbrot_80k.c cria_estado_inicial.o rotinas_comuns.o nivel_3.o nivel_2.o nivel_1.o mt19937ar.o
	gcc $(CFLAGS) -o $@ $^

clean:
	del *.exe
	del *.o
