#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "rotinas_comuns.h"

#define NOME_ARQUIVO_BMP "mandel80k.bmp"
#define NOME_ARQUIVO_TMP "mandel80k.bmp.tmp"
#define QTD_LINHAS_FRACTAL (80001)
#define QTD_COLUNAS_FRACTAL (QTD_LINHAS_FRACTAL)
#define QTD_LINHAS_CALCULAR ((QTD_LINHAS_FRACTAL - 1) / 2)
#define DIMENSAO_PIXEL (4.0 / QTD_LINHAS_FRACTAL)

#define QTD_TESTES (1000000)

extern bool is_bloco_preto_nivel_1 (int linha, int coluna);
extern void cria_estado_inicial(void);

static uint8_t *linha_cores = NULL;
static uint8_t *buffer_gravacao = NULL;

void atualiza_linha(int);
void atualiza_linha_atual(int);

int le_linha_atual (void) {
	FILE *fd;
	int linha_atual;
	/* ler linha atual */
	fd = fopen ("linha_atual.dat", "r");
	if (fd == NULL) {
		fprintf(stderr, "Erro ao abrir linha_atual.dat\n");
		exit(1);
	}
	fscanf (fd, "%d", &linha_atual);
	fclose (fd);
	if (linha_atual < 0 || linha_atual > QTD_LINHAS_CALCULAR) {
		fprintf (stderr, "Valor invalido de linha_atual: %d\n", linha_atual);
		exit(1);
	}
	return linha_atual;
}

void verifica_parada_execucao (void) {
	if (! file_exists("stop.txt")) return;
	printf ("\nSolicitada parada na execucao (presenca de stop.txt)\n");
	(void) remove ("stop.txt");
	if (linha_cores != NULL) free (linha_cores);
	if (buffer_gravacao != NULL) free (buffer_gravacao);
	exit(0);
}

void verifica_existencia_fractal (void) {
	if (! file_exists(NOME_ARQUIVO_BMP)) return;
	fprintf (stderr, "Fractal ja existe. Apague %s se quiser reiniciar.\n", NOME_ARQUIVO_BMP);
	exit(1);
}

void trata_reexecucao (void) {
	if (!
		(
			file_exists("linha_atual.dat") &&
			file_exists("nivel1.dat") &&
			file_exists("nivel2.dat") &&
			file_exists("nivel3.dat") &&
			file_exists(NOME_ARQUIVO_TMP)
		)
	) {
		fprintf (stderr, "Esta faltando algum arquivo .dat ou .tmp. Apague os que existirem e recomece do inicio.\n");
		exit(1);
	}
}

void trata_primeira_execucao (void) {
	if (	file_exists("linha_atual.dat") ||
			file_exists("nivel1.dat") ||
			file_exists("nivel2.dat") ||
			file_exists("nivel3.dat") ||
			file_exists(NOME_ARQUIVO_TMP)
	) {
		fprintf (stderr, "Existe alaum arquivo .dat ou .tmp de execucao anterior. Apague os que existirem e recomece.\n");
		exit(1);
	}
	cria_estado_inicial();
}

void trata_primeira_execucao_ou_reexecucao (void) {
	if (file_exists("linha_atual.dat")) trata_reexecucao();
	else trata_primeira_execucao();
}

int main (void) {
	int linha_atual;
	int i;
	
	verifica_existencia_fractal(); /* trava de seguranca para nao sobrescrever fractal pronto */

	verifica_parada_execucao();
	
	trata_primeira_execucao_ou_reexecucao();
	
	linha_atual = le_linha_atual();
	
	linha_cores = (uint8_t *) malloc(QTD_LINHAS_FRACTAL);
	buffer_gravacao = (uint8_t *) malloc((QTD_LINHAS_FRACTAL / 2) + 5);
	if (linha_cores == NULL || buffer_gravacao == NULL) {
		if (linha_cores != NULL) free (linha_cores);
		if (buffer_gravacao != NULL) free (buffer_gravacao);
		fprintf (stderr, "Sem memoria.\n");
		exit(1);
	}

	printf ("Desenhando linhas do fractal.\n");
	for (i = linha_atual; i < QTD_LINHAS_CALCULAR; i++) {
		printf ("\rProgresso: %7.3f%%", 100.0 * (1 + i * 2) / QTD_LINHAS_FRACTAL);
		fflush (stdout);
		verifica_parada_execucao();
		atualiza_linha(i);
		linha_atual++;
		atualiza_linha_atual(linha_atual);
	}
	printf ("\rProgresso: 100.000%%\n");

	(void) rename(NOME_ARQUIVO_TMP, NOME_ARQUIVO_BMP);
	(void) remove("nivel1.dat");
	(void) remove("nivel2.dat");
	(void) remove("nivel3.dat");
	(void) remove("linha_atual.dat");
	(void) remove("stop.txt");

	printf ("Gerado fractal em %s\n", NOME_ARQUIVO_BMP);
	if (linha_cores != NULL) free (linha_cores);
	if (buffer_gravacao != NULL) free (buffer_gravacao);

	return 0;
}

void atualiza_linha (int n_linha) {
	int64_t posicao_seek;
	FILE *fd;
	int j;
	double x, y;
	int32_t tamanho_linha_bytes;
	const int metade_colunas = (QTD_COLUNAS_FRACTAL - 1) / 2;
	const int metade_linhas = (QTD_LINHAS_FRACTAL - 1) / 2;

	const int qtd_cores = 16;
	const int bits_por_pixel = 4;
	const int tamanho_cabecalho_bmp = 14;
	const int tamanho_header_versao_bmp = 40; /* tamanho BITMAPINFOHEADER */
	const int bytes_por_cor = 4;
	
	y = (n_linha + 1) * DIMENSAO_PIXEL; /* esse +1 é porque a numeração da parte dos blocos reinicia em 0, mas já houve a linha central gravada nos cálculos iniciais. */
	/* ultima coluna é calculada sempre à parte */
	for (j = QTD_COLUNAS_FRACTAL - 2; j >= 0; j--) {
		if (is_bloco_preto_nivel_1 (n_linha / 10, j / 10)) linha_cores[j] = 0;
		else {
			/* OBS: x mira no centro do pixel. Diferente do cálculo dos blocos, em que era calculado o x
			do início real do bloco. Por isso neste caso não se subtrai metade da dimensão do pixel. */
			x = (j - metade_colunas) * DIMENSAO_PIXEL;
			linha_cores[j] = cor_pixel(x, y, QTD_TESTES);
		}
	}
	/* cálculo da última coluna, que não usa os blocos de otimização (mas não tem necessidade, pois sempre sai rápido) */
	x = 2.0 - (DIMENSAO_PIXEL / 2.0);
	linha_cores[QTD_COLUNAS_FRACTAL - 1] = cor_pixel(x, y, QTD_TESTES);
	fd = fopen(NOME_ARQUIVO_TMP, "rb+");
	if (fd == NULL) {
		fprintf (stderr, "Erro ao abrir %s para atualizacao.\n", NOME_ARQUIVO_TMP);
		exit (1);
	}
	
	tamanho_linha_bytes = ((QTD_COLUNAS_FRACTAL * bits_por_pixel + 31) / 32) * 4; /* Para 80001 colunas, dá 40004*/

	/* linha inferior (bitmap guarda em ordem contraria) */
	posicao_seek = (int64_t) (tamanho_cabecalho_bmp + tamanho_header_versao_bmp + (qtd_cores * bytes_por_cor)); /* cabeçalhos do bitmap, falta somar o deslocamento das linhas */
	posicao_seek += (int64_t)(metade_linhas - 1 - n_linha) * (int64_t) tamanho_linha_bytes;
	/* _fseeki64(fd, (__int64) posicao_seek, SEEK_SET); / * versao de fseek com 64 bits que não funcionou*/
	fseeko64 (fd, posicao_seek, SEEK_SET); /* versao de fseek com 64 bits */
	grava_linha_bmp_4bpp (fd, linha_cores, buffer_gravacao, QTD_COLUNAS_FRACTAL); /* junta duas cores em cada byte */
	
	/* linha superior (bitmap guarda em ordem contraria) */
	posicao_seek = (int64_t) (tamanho_cabecalho_bmp + tamanho_header_versao_bmp + (qtd_cores * bytes_por_cor)); /* cabeçalhos do bitmap, falta somar o deslocamento das linhas */
	posicao_seek += (int64_t)(metade_linhas + 1 + n_linha) * (int64_t) tamanho_linha_bytes;
	fseeko64 (fd, posicao_seek, SEEK_SET); /* versao de fseek com 64 bits */
	fwrite (buffer_gravacao, tamanho_linha_bytes, 1, fd); /* gravar direto pois as cores já foram juntadas em buffer_gravacao */

	fclose (fd);
}

/* atualizar a linha atual no arquivo */
void atualiza_linha_atual (int linha_atual) {
	FILE *fd;
	fd = fopen("linha_atual.dat", "w");
	if (fd == NULL) {
		fprintf(stderr, "Erro ao abrir linha_atual.dat para atualizacao.\n");
		exit(1);
	}
	fprintf (fd, "%d\n", linha_atual);
	fclose (fd);
}
