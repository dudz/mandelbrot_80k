#ifndef __ROTINAS_COMUNS_H
#define __ROTINAS_COMUNS_H

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "mt19937ar.h"

/* rotinas para 64 bits, mas n�o funcionou. Foi usafo fseeko64 */
/*
extern "C" int __cdecl _fseeki64(FILE *, __int64, int);
extern "C" __int64 __cdecl _ftelli64(FILE *);
*/

uint8_t cor_pixel(double x, double y, int32_t qtd_testes);
bool file_exists(char *filename);
void grava_linha_bmp_4bpp (FILE *fd, uint8_t *linha, uint8_t *buffer_gravacao, int tamanho);

#endif
