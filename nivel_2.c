/*
Cria estado inicial do nível 2 (imagem 800x800) do fractal mandelbrot.

Ver nivel 3 para o formato e descrição mais detalhada.

*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "rotinas_comuns.h"

#define NOME_ARQUIVO "nivel2.dat"
#define QTD_LINHAS_BLOCO (800)
#define QTD_COLUNAS_BLOCO QTD_LINHAS_BLOCO
#define QTD_LINHAS_FRACTAL (80001)
#define DIMENSAO_PIXEL (4.0 / QTD_LINHAS_FRACTAL)
#define DIMENSAO_BLOCO ((4.0 - DIMENSAO_PIXEL) / QTD_LINHAS_BLOCO)

#define TAMANHO_LADO_BLOCO_NIVEL_ANTERIOR (10) /* quantidade de blocos de maior granularidade que compõem o lado do bloco */

#define QTD_TESTES (10000)

static uint8_t *linhas[5];
static int linha_central = -1;

extern bool is_bloco_preto_nivel_3 (int linha, int coluna);

/* 1 = candidato a bloco inteiro dentro do conjunto
   2 = bloco tem pontos fora do conjunto
*/
static double calcula_cor_bloco(double x, double y) {
	double espacamento = DIMENSAO_BLOCO / 4.0;
	int i, j;
	double xx, yy;
	uint8_t cor;

	/* testar o bloco numa grade 5x5 */
	for (i = 0; i < 5; i++) {
		yy = y + (espacamento * i);
		for (j = 0; j < 5; j++) {
			xx = x + (espacamento * j);
			cor = cor_pixel(xx, yy, QTD_TESTES);
			if (cor != 0) return 2;
		}
	}
	/* testar pontos aleatórios do bloco */
	for (i = 2 * TAMANHO_LADO_BLOCO_NIVEL_ANTERIOR; i > 0; i--) {
		xx = x + (genrand_res53() * DIMENSAO_BLOCO);
		yy = y + (genrand_res53() * DIMENSAO_BLOCO);
		cor = cor_pixel(xx, yy, QTD_TESTES);
		if (cor != 0) return 2;
	}
	return 1;
}

void estado_inicial_nivel_2 (void) {
	FILE *fd;
	int i, j;
	int ii, jj;
	double x, y;
	bool is_bloco_dentro_fractal;
	
	if (file_exists(NOME_ARQUIVO)) {
		fprintf (stderr, "%s ja existe. Apague se quiser reiniciar.\n", NOME_ARQUIVO);
		exit(1);
	}
	for (i = 0; i < 5; i++) {
		linhas[i] = (uint8_t *) malloc(QTD_COLUNAS_BLOCO + 1);
		if (linhas[i] == NULL) {
			fprintf (stderr, "Sem memoria.\n");
			exit(1);
		}
		linhas[i][QTD_COLUNAS_BLOCO] = '\n';
	}
	fd = fopen(NOME_ARQUIVO, "wb");
	if (fd == NULL) {
		fclose (fd);
		fprintf (stderr, "Erro ao criar %s\n", NOME_ARQUIVO);
		exit(1);
	}

	/* calcular cores das linhas de blocos */
	for (i = 0; i < 3; i++) {
		/* (x,y) marca o canto superior esquerdo do bloco */
		y = (DIMENSAO_PIXEL / 2.0) + (i * DIMENSAO_BLOCO);
		for (j = 0; j < QTD_COLUNAS_BLOCO; j++) {
			printf ("\r%6.2f%%", 100.0 * (i * QTD_COLUNAS_BLOCO + j) / (3 * QTD_COLUNAS_BLOCO));
			fflush (stdout);
			if (is_bloco_preto_nivel_3 (i / 10, j / 10)) linhas[i + 2][j] = 0;
			else {
				x = (j - (QTD_COLUNAS_BLOCO / 2)) * DIMENSAO_BLOCO - (DIMENSAO_PIXEL / 2.0);
				linhas[i + 2][j] = calcula_cor_bloco(x, y);
			}
		}
	}
	printf ("\rGravando arquivo...");
	fflush (stdout);
	/* copiar linhas iguais */
	for (j = 0; j < QTD_COLUNAS_BLOCO; j++) {
		linhas[1][j] = linhas[2][j];
		linhas[0][j] = linhas[3][j];
	}

	/* ver quais blocos da linha central podem ser considerados completamente dentro do fractal.
		Será considerado interno ao fractal caso todos os blocos vizinhos com distância num
		quadrado 2x2 sejam blocos candidatos a serem internos ao fractal.
	*/
	for (j = 2; j < QTD_COLUNAS_BLOCO - 2; j++) {
		if (linhas[2][j] == 2) continue;
		is_bloco_dentro_fractal = true;
		for (ii = 0; ii < 5 && is_bloco_dentro_fractal; ii++) {
			for (jj = j - 2; jj <= j + 2 && is_bloco_dentro_fractal; jj++) {
				if (linhas[ii][jj] == 2) is_bloco_dentro_fractal = false;
			}
		}
		if (is_bloco_dentro_fractal) linhas[2][j] = 0;
	}

	/* transformar o conteúdo em ascii */
	for (i = 0; i < 5; i++) {
		for (j = 0; j < QTD_COLUNAS_BLOCO; j++) {
			linhas[i][j] += '0';
		}
	}

	/* gravar no arquivo */
	fprintf (fd, "%d\n", 0);
	for (i = 0; i < 5; i++) {
		fwrite (linhas[i], QTD_COLUNAS_BLOCO + 1, 1, fd);
	}

	fclose (fd);
	for (i = 0; i < 5; i++) {
		free (linhas[i]);
		linhas[i] = NULL;
	}
	printf ("\rArquivo gerado       \n");
}

static void carrega_estado_nivel_2 (void) {
	FILE *fd;
	int i, j;
	char buf[16];
	
	for (i = 0; i < 5; i++) {
		linhas[i] = (uint8_t *) malloc(QTD_COLUNAS_BLOCO + 2);
		if (linhas[i] == NULL) {
			fprintf (stderr, "Sem memoria.\n");
			exit(1);
		}
	}

	fd = fopen (NOME_ARQUIVO, "r");
	if (fd == NULL) {
		fprintf (stderr, "Erro ao abrir %s\n", NOME_ARQUIVO);
		exit(1);
	}
	
	fgets(buf, sizeof(buf), fd);
	sscanf(buf, "%d", &linha_central);

	for (i = 0; i < 5; i++) {
		fgets((char *) linhas[i], QTD_COLUNAS_BLOCO + 2, fd);
		linhas[i][QTD_COLUNAS_BLOCO] = '\n';
		linhas[i][QTD_COLUNAS_BLOCO + 1] = '\0';
		for (j = 0; j < QTD_COLUNAS_BLOCO; j++) linhas[i][j] -= '0';
	}

	fclose (fd);
}

static void incrementar_janela_linhas (void) {
	uint8_t *ptr_tmp;
	int i, j;
	int ii, jj;
	double x, y;
	bool is_bloco_dentro_fractal;
	FILE *fd;

	/* rotacionar linhas */
	ptr_tmp = linhas[0];
	for (i = 0; i < 4; i++) linhas[i] = linhas[i + 1];
	linhas[4] = ptr_tmp;
	
	linha_central++;
	
	/* calcular nova ultima linha */
	/* (x,y) marca o canto superior esquerdo do bloco */
	i = linha_central + 2;
	y = (DIMENSAO_PIXEL / 2.0) + (i * DIMENSAO_BLOCO);
	for (j = 0; j < QTD_COLUNAS_BLOCO; j++) {
		if (is_bloco_preto_nivel_3 (i / 10, j / 10)) linhas[4][j] = 0;
		else {
			x = (j - (QTD_COLUNAS_BLOCO / 2)) * DIMENSAO_BLOCO - (DIMENSAO_PIXEL / 2.0);
			linhas[4][j] = calcula_cor_bloco(x, y);
		}
	}

	/* ver quais blocos da linha central podem ser considerados completamente dentro do fractal. */
	for (j = 2; j < QTD_COLUNAS_BLOCO - 2; j++) {
		if (linhas[2][j] == 2) continue;
		is_bloco_dentro_fractal = true;
		for (ii = 0; ii < 5 && is_bloco_dentro_fractal; ii++) {
			for (jj = j - 2; jj <= j + 2 && is_bloco_dentro_fractal; jj++) {
				if (linhas[ii][jj] == 2) is_bloco_dentro_fractal = false;
			}
		}
		if (is_bloco_dentro_fractal) linhas[2][j] = 0;
	}

	/* transformar o conteúdo em ascii */
	for (i = 0; i < 5; i++) {
		for (j = 0; j < QTD_COLUNAS_BLOCO; j++) {
			linhas[i][j] += '0';
		}
	}

	fd = fopen (NOME_ARQUIVO, "wb");
	if (fd == NULL) {
		fprintf (stderr, "Erro ao recriar %s\n", NOME_ARQUIVO);
		exit(1);
	}

	/* gravar no arquivo */
	fprintf (fd, "%d\n", linha_central);
	for (i = 0; i < 5; i++) {
		linhas[i][QTD_COLUNAS_BLOCO] = '\n';
		fwrite (linhas[i], QTD_COLUNAS_BLOCO + 1, 1, fd);
	}

	fclose (fd);

	/* transformar o conteúdo em numeros normais (nao ascii) */
	for (i = 0; i < 5; i++) {
		for (j = 0; j < QTD_COLUNAS_BLOCO; j++) {
			linhas[i][j] -= '0';
		}
	}
}


/* recebe linha e coluna dos blocos do nivel 2, e devolve true se for bloco contido no
conjunto preto do fractal
*/
bool is_bloco_preto_nivel_2 (int linha, int coluna) {
	if (linha_central < 0) carrega_estado_nivel_2();
	
	if (linha < linha_central) {
		fprintf (stderr, "Requisitada linha %d mas linha central esta mais adiante (%d).\n", linha, linha_central);
		exit (1);
	}
	else if (linha > linha_central + 1) {
		fprintf (stderr, "Requisitada linha %d mas linha central esta atrasada mais de uma unidade (%d).\n", linha, linha_central);
		exit (1);
	}
	
	if (linha == linha_central + 1) {
		incrementar_janela_linhas();
	}
	
	if (linha != linha_central) {
		fprintf (stderr, "Requisitada linha %d mas linha central eh diferente (%d).\n", linha, linha_central);
		exit (1);
	}
	
	if (linhas[2][coluna] == 0) return true;
	return false;
}
